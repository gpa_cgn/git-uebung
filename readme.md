# git cheat sheet

Read your group instruction in the text files 

[group1.md](group1.md)

[group2.md](group2.md)

[group3.md](group3.md)

[group4.md](group4.md)

and place your solution in a fitting category in this file. It's best to create a branch while working on the exercise!

## Creating repositories

    $ git init                    Erzeugt ein lokales Repository
	$ git clone https://...       Lädt ein Remote-Repository in ein lokales Verzeichnis und erzeugt ein lokales Repository

## Staging and committing

    $ git add test.txt            Stellt die angebene Datei auf die Stage. (Merkt sie für den nächsten Commit vor.)
	$ git add .                   Staged alle Dateien, die sich im aktuellen Verzeichnis befinden
    $ git add -u                  Staged alle bekannten Dateien im ganzen Tree.
    $ git add -A                  Staged alle Dateien im Tree (also auch neue Dateien).
	
## Inspecting the repository and history


    $ git cat-file -p a3798b  -> zeigt den Inhalt des Objekts mit dem angegebenen Commit-Hash an
    $ git diff                                      Shows the changes made
    $ git diff --staged                             Shows the difference between files in the staged area
    $ git diff test.txt                             Shows changes made in test.txt
    $ git diff --theirs                             Shows difference relative to the not HEAD branch
    $ git diff --ours                               Shows difference relative to the current branch
    $ git log                                       Shows a history of the last commits
    $ git log --oneline                             The same as log, but in one line
    $ git log --oneline --all                       The same as log --oneline, but for all branches
    $ git log --oneline --all --graph               Graphical representation of the commits in all branches in one line
    $ git log --oneline --all --graph --decorate    Prints additionally ref names of the commits
    $ git log --follow -p -- filename               Entire history of one file before and after renames
    $ git log -S'static void Main'                  Searches for the string 'static void Main'
    $ git log --pretty=format:"%h - %an, %ar : %s"  Prints a formatted log: %h-abbreviated commit hash; %an-author name; %ar-realative author date; %s-subject


## Managing branches

    $ git checkout master   Switches to master branch
	$ git branch feature1   Creates feature1 branch
	$ git checkout -b work  Creates work branch and switches to work
	$ git checkout work     Switches to work branch
    $ git branch			Erstellt ausgehend vom aktuellen Stand des Repos einen neuen Branch.
    $ git branch -v			shows commit subject line and sha1 of each HEAD.
    $ git branch -a			Option -a shows both local and remote branches. active branch is marked with asterix.
	$ git branch --merged   Shows branches merged in the past
	$ git branch --no-merged    Shows branches which are not merged yet
	$ git branch -d work    Deletes branch work, if it's merged in its upstream branch
	$ git branch -D work    Deletes branch work, even if it's not merged (-d --force)
	
	$ git checkout work   -> wechselt auf den Branch work
    $ git checkout master -> wechselt auf den Branch master


## Merging

    $ git merge work  -> merged den aktuell gewählten Branch mit dem Branch work
    $ git merge --abort  -> geht auf den Zustand vor dem Merge zurück

Example 1:

    A - B (master)
         \
          C (work)
    
    $ git checkout master
    $ git merge work

Result and explanation here:
Wechsel auf den Branch master, Zustand B
Merge dort mit dem Branch work (fast forward merge)
Ergebnis: Master ist auf dem Stand C

Example 2:

    A - B - C - D (master)
         \
          X - Y (work)
    
    $ git checkout master
    $ git merge work

Result and explanation here:
Wechsel auf den Branch master, Zustand D
Merge dort mit dem Branch work
Ergebnis: D und Y sind auf Branch master zu neuem Zustand DY zusammengeführt.
Branch work verbleibt auf Zustand Y.

Example 3:

    A - B - C - D (master)
         \
          X - Y (work)
    
    $ git checkout work (1)
    $ git merge master (2)
    $ git checkout master (3)
    $ git merge work (4)

Result and explanation here:
(1) Wechsel auf den Branch work, Zustand Y
(2) Merge dort mit dem Branch master
Ergebnis: D und Y sind auf Branch work zu neuem Zustand DY zusammengeführt.
Branch master verbleibt auf Zustand D.
(3) Wechsel auf den Branch master, Zustand D
(4) Merge dort mit dem Branch work
Ergebnis: Branch master ist in Zustand DY.

    A - B - C - D     
         \       \
          X - Y - DY   (work, master)

test